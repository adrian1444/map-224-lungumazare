import domain.Friendship;
import domain.Message;
import domain.Tuple;
import domain.User;
import domain.validators.FriendshipValidator;
import domain.validators.MessageValidator;
import domain.validators.UserValidator;
import domain.validators.ValidationException;
import repository.Repository;
import repository.db.FriendshipDbRepository;
import repository.db.MessageDbRepository;
import repository.db.UserDbRepository;
import repository.file.FriendshipFile;
import repository.file.UserFile;
import repository.memory.InMemoryRepository;
import service.Service;
import ui.UI;

import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;

public class Main {

    public static void main(String[] args) {
        String userFileName = "data/users.csv";
        String friendshipFileName = "data/friendships.csv";
       // try{
            //Repository<Long, User> userFileRepository = new UserFile(userFileName, new UserValidator());
            //Repository<Tuple<Long,Long>, Friendship> friendshipFileRepository = new FriendshipFile(friendshipFileName, new FriendshipValidator());
            //Service service = new Service(userFileRepository, friendshipFileRepository);
            //UI ui = new UI(service);
            //ui.run();

        //}catch(FileNotFoundException e)
        //{
        //   e.printStackTrace();
        //}


        Repository<Long, User> userDbRepository = new UserDbRepository("jdbc:postgresql://localhost:5432/ReteaDeSocializare", "postgres", "xela160302", new UserValidator());
        Repository<Tuple<Long, Long>, Friendship> friendshipDbRepository = new FriendshipDbRepository("jdbc:postgresql://localhost:5432/ReteaDeSocializare", "postgres", "xela160302", new FriendshipValidator());
        MessageDbRepository messageDbRepository=new MessageDbRepository("jdbc:postgresql://localhost:5432/ReteaDeSocializare", "postgres", "xela160302", new MessageValidator(),userDbRepository);
        Service service = new Service(userDbRepository, friendshipDbRepository,messageDbRepository);
        UI ui = new UI(service);
        ui.run();
    }
}
