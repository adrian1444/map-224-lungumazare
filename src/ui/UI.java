package ui;
import domain.Friendship;
import domain.Message;
import domain.Tuple;
import domain.User;
import domain.validators.ValidationException;
import service.Service;
import service.ServiceException;

import java.sql.SQLOutput;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class UI {
    private Service service;

    public UI(Service service) {

        this.service = service;
    }
    private void addUserFromConsole(){
        Scanner input = new Scanner(System.in);
        String lastName = null;
        String firstName = null;
        String ID = null;
        System.out.println("Introduce first name: \n");
        firstName = input.next();
        System.out.println("Introduce last name: \n");
        lastName = input.next();
        try{
            User user = new User(firstName, lastName);
            service.addUser(user);
            System.out.println("User added successfully");
        }catch (NumberFormatException ex){
            System.out.println("ID should be a positive number");
        }catch (ValidationException ex){
            System.out.println(ex.toString());
        }

    }
    private void deleteUserFromConsole(){
        Scanner input = new Scanner(System.in);
        String id = null;
        System.out.println("Introduce ID: \n");
        id = input.next();
        try {
            Long ID = Long.parseLong(id);
            service.deleteUser(ID);
            System.out.println("User deleted successfully");

        }catch(NumberFormatException ex){
            System.out.println("ID should be a positive number");
        }catch (ServiceException ex){
            System.out.println(ex.getMessage());
        }
    }

    private void printAllUsers(){
        Iterable<User> users = service.getAllUsers();
        for(User user : users)
            System.out.println(user.toString());

    }

    private void addFriendshipFromConsole(){

        Scanner input = new Scanner(System.in);
        String id1 = null;
        String id2 = null;
        System.out.println("Introduce ID 1: ");
        id1 = input.next();
        System.out.println("Introduce ID 2: ");
        id2 = input.next();
        try{
           Long ID1 = Long.parseLong(id1);
           Long ID2 = Long.parseLong(id2);

           if (ID1 > ID2) {
               Long swap = ID1;
               ID1 = ID2;
               ID2 = swap;
           }

           Friendship friendship = new Friendship(ID1, ID2);
           service.addFriendship(friendship);
           System.out.println("Friendship added successfully\n");

        }catch(NumberFormatException ex){
            System.out.println("IDs should be positive numbers\n");
        }catch(ValidationException ex){
            System.out.println(ex.toString());
        }catch (ServiceException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public void deleteFriendshipFromConsole(){
        Scanner input = new Scanner(System.in);
        String id1 = null;
        String id2 = null;
        System.out.println("Introduce ID 1: ");
        id1 = input.next();
        System.out.println("Introduce ID 2: ");
        id2 = input.next();
        try {
            Long ID1 = Long.parseLong(id1);
            Long ID2 = Long.parseLong(id2);
            if (ID1 > ID2) {
                Long swap = ID1;
                ID1 = ID2;
                ID2 = swap;
            }
            Friendship friendship = new Friendship(ID1, ID2);
            service.deleteFriendship(friendship.getId());
            System.out.println("Friendship deleted successfully\n");

        }catch(NumberFormatException ex){
            System.out.println("IDs should be positive numbers\n");
        }catch(ValidationException ex){
            System.out.println(ex.toString());
        }catch(ServiceException ex){
            System.out.println(ex.getMessage());
        }

    }

    public void printAllFriendships(){
        Iterable<Friendship> friendships = service.getAllFriendships();
        for(Friendship friendship : friendships)
            System.out.println(friendship.getId().toString());
    }
    public void numberOfComunities(){
       int numberOfComunities = service.numberOfComunities();
       System.out.println("Number of comunities: "+numberOfComunities);
    }

    public void mostSociableComunity(){
        List<Long> result = service.mostSociableComunity();
        for (Long memberId : result){
            System.out.print('"' + service.findOneUser(memberId).getFirstName()+" "+ service.findOneUser(memberId).getLastName()+'"' +" ");
        }
        System.out.print('\n');
    }

    public void printAllFriendsFromUser() {
        Scanner input = new Scanner(System.in);
        String id = null;
        System.out.println("Introduce the user's ID:\n");
        id = input.next();
        try {
            Long ID = Long.parseLong(id);
            List<User> friends=service.getUserFriends(ID);
            Long ID2,aux;
            for(User user: friends){
                ID2= user.getId();
                if(ID> ID2){
                    aux=ID2;
                    ID2=ID;
                    ID=aux;
                }

                Friendship friendship=service.findOneFriendship(new Tuple<Long,Long>(ID, ID2));
                System.out.println(user.toString() + " " + friendship.getDate());
            }


        } catch (NumberFormatException ex){
            System.out.println("IDs should be positive numbers\n");
        } catch(ServiceException ex){
            System.out.println(ex.getMessage());
        }

    }
    public void printAllFriendsFromUserByGivenMonth() {
        Scanner input = new Scanner(System.in);
        String id = null;
        String month = null;
        System.out.println("Introduce the user's ID: \n");
        id = input.next();
        System.out.println("Introduce the month: \n");
        month = input.next();
        try {
            Long ID = Long.parseLong(id);
            List<User> friends=service.getUserFriendsByGivenMonth(ID,month);
            Long ID2,aux;
            for(User user: friends){
                ID2= user.getId();
                if(ID> ID2){
                    aux=ID2;
                    ID2=ID;
                    ID=aux;
                }

                Friendship friendship=service.findOneFriendship(new Tuple<Long,Long>(ID, ID2));
                System.out.println(user.toString() + " " + friendship.getDate());
            }


        } catch (NumberFormatException ex){
            System.out.println("IDs should be positive numbers\n");
        } catch(ServiceException ex){
            System.out.println(ex.getMessage());
        }catch(ValidationException ex){
            System.out.println(ex.toString());
        }

    }

    private void login(){
        Scanner input = new Scanner(System.in);
        String id = null;
        System.out.println("Introduce the user's ID: \n");
        id= input.next();
        try {
            Long ID = Long.parseLong(id);
            service.checkUserExistence(ID);
            loginMenu(ID);
        } catch (NumberFormatException ex) {
            System.out.println("IDs should be positive numbers\n");
        }catch(ServiceException ex){
            System.out.println(ex.getMessage());
        }

    }

    private void replyToMessage(Long Id){
        List<Message> messages=service.unrepliedMessages(Id);
        if(messages.size()==0){
            System.out.println("There are no unreplied messages!");
            return;
        }
        System.out.println("List of unreplied messages:");
        for(Message message: messages)
            System.out.println("Id="+String.valueOf(message.getId())+" "+message.getFrom().getFirstName()+" "+message.getFrom().getLastName()+" "+message.getMessageText()+" "+message.getDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm")).toString());
        Scanner input = new Scanner(System.in);
        String idMessage = null;
        System.out.println("Introduce the id of the message you want to reply:");
        idMessage= input.next();
        try {
            Long IDmessage = Long.parseLong(idMessage);
            if(!messages.contains(service.findOneMessage(IDmessage))) {
                System.out.println("Invalid message id\n");
                return;
            }
            System.out.println("Introduce the reply message:");
            String replyMessage=input.next();
            service.replyToOneUser(Id,IDmessage,replyMessage);
        } catch (NumberFormatException ex) {
            System.out.println("IDs should be positive numbers\n");
        } catch(ServiceException ex){
            System.out.println(ex.getMessage());
        }

    }

    private void seeMessagesBetweenTwoUsers(Long Id1){
        Scanner input = new Scanner(System.in);
        System.out.println("Introduce the id of the second user:\n");
        String id2=input.next();
        try{
            Long Id2=Long.parseLong(id2);
            List<Message> messages=service.seeMessagesBetweenTwoUsers(Id1,Id2);
            for(Message message: messages)
                System.out.println(message.getFrom().getFirstName()+": "+message.getMessageText()+" "+message.getDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm")).toString()+"\n");
        }catch(NumberFormatException ex) {
                System.out.println("The command should be a number!\n");
        }catch(ServiceException ex){
            System.out.println(ex.getMessage());
        }

    }

    public void writeMessage(Long idWriter){
        Scanner input = new Scanner(System.in);
        String receiversAsString="";
        while(true) {
            try {
                System.out.println("Press stop after you finish.\n");
                System.out.println("Introduce the id of the receiver:\n");
                String id = input.next();
                if (id.equals("stop"))
                    break;
                receiversAsString=receiversAsString+id+";";
                Long ID = Long.parseLong(id);
            }catch(NumberFormatException ex){
                System.out.println("The id should be a number!\n");
                break;
            }
        }
        receiversAsString=receiversAsString.substring(0, receiversAsString.length() - 1);
        System.out.println("Introduce the message: ");
        String messageText=input.next();
        try{
            service.writeMessage(idWriter,receiversAsString,messageText);
        }catch(ServiceException ex){
            System.out.println(ex.getMessage());
        }
    }
    public void sendFriendRequest(Long idFrom){
        Scanner input = new Scanner(System.in);
        System.out.println("Introduce the id of the user: ");
        String idTo = input.next();
        try{
            Long IDTo = Long.parseLong(idTo);
            service.sendFriendRequest(idFrom, IDTo);

        }catch (NumberFormatException ex){
            System.out.println("ID should be a positive number!\n");
        }catch(ServiceException ex){
            System.out.println(ex.getMessage());
        }
    }

    public void responseFriendRequest(Long idFrom){
        List<Friendship> pendingFriendships = service.pendingFriendships(idFrom);
        if(pendingFriendships.size() == 0){
            System.out.println("There are no friend requests\n");
            return;
        }
        System.out.println("List of friend requests:");
        for(Friendship friendship: pendingFriendships){
            if(idFrom.equals(friendship.getId().getLeft()))
                System.out.println("Id="+String.valueOf(friendship.getId().getRight()) + " " + service.findOneUser(friendship.getId().getRight()).getLastName() + " " + service.findOneUser(friendship.getId().getRight()).getFirstName());
            if(idFrom.equals(friendship.getId().getRight()))
                System.out.println("Id="+String.valueOf(friendship.getId().getLeft()) + " " + service.findOneUser(friendship.getId().getLeft()).getLastName() + " " + service.findOneUser(friendship.getId().getLeft()).getFirstName());
        }
        Scanner input = new Scanner(System.in);
        String idUser = null;
        System.out.println("Introduce the id of the user you want to response:");
        idUser = input.next();
        try {
            Long IDUser = Long.parseLong(idUser);
            boolean verify = false;
            Long id1 = idFrom;
            Long id2 = IDUser;
            if(idFrom > IDUser){
                Long swap = idFrom;
                idFrom = IDUser;
                IDUser = swap;
                verify = true;
            }
            if(!pendingFriendships.contains(service.findOneFriendship(new Tuple<Long, Long> (idFrom, IDUser)))) {
                System.out.println("Invalid friend request\n");
                return;
            }
            System.out.println("Type 'approved' or 'rejected': ");
            String status = input.next();
            if(!status.equals("approved") && !status.equals("rejected")){
                System.out.println("Invalid response!\n");
                return;
            }

            service.responseToFriendRequest( id1, id2, status);

        } catch (NumberFormatException ex) {
            System.out.println("IDs should be positive numbers\n");
        } catch(ServiceException ex){
            System.out.println(ex.getMessage());
        }

    }

    private void loginMenu(Long ID){
        System.out.println("Options:\n");
        System.out.println("1. Write a new message\n");
        System.out.println("2. See conversation with a certain user\n");
        System.out.println("3. Reply to a message\n");
        System.out.println("4. Send friend request\n");
        System.out.println("5. Response to a friend request friend request\n");
        System.out.println("6. Logout\n");

        while(true) {
            System.out.println(">>>");
            String cmd = null;
            int command;
            Scanner input = new Scanner(System.in);
            cmd = input.next();
            try {
                command = Integer.parseInt(cmd);
                if (command == 1)
                    writeMessage(ID);
                else if (command == 2)
                    seeMessagesBetweenTwoUsers(ID);
                else if (command == 3)
                    replyToMessage(ID);
                else if (command == 4)
                    sendFriendRequest(ID);
                else if (command == 5)
                    responseFriendRequest(ID);
                else if (command == 6)
                    break;
                else
                    System.out.println("Invalid command");
            } catch (NumberFormatException ex) {
                System.out.println("The command should be a number!\n");
            }
        }
    }

    public void run(){
        System.out.println("Options:\n");
        System.out.println("1. Add user\n");
        System.out.println("2 .Delete user\n");
        System.out.println("3. Print all users\n");
        System.out.println("4. Add friendship\n");
        System.out.println("5. Delete friendship\n");
        System.out.println("6. Print all friendships\n");
        System.out.println("7. Number of comunities\n");
        System.out.println("8. The most sociable comunity\n");
        System.out.println("9. Show friends of a user\n");
        System.out.println("10. Show friends of a user whose friendships were created in a given month\n");
        System.out.println("11.Login\n");
        System.out.println("12. Exit");

        while(true){
            System.out.println(">>>");
            String cmd = null;
            int command;
            Scanner input = new Scanner(System.in);
            cmd = input.next();
            try{
                command = Integer.parseInt(cmd);
                if(command == 1)
                    addUserFromConsole();
                else if(command == 2)
                    deleteUserFromConsole();
                else if(command == 3)
                    printAllUsers();
                else if(command == 4)
                    addFriendshipFromConsole();
                else if(command == 5)
                    deleteFriendshipFromConsole();
                else if(command == 6)
                    printAllFriendships();
                else if(command ==7)
                    numberOfComunities();
                else if(command == 8)
                    mostSociableComunity();
                else if(command == 9)
                    printAllFriendsFromUser();
                else if(command == 10)
                    printAllFriendsFromUserByGivenMonth();
                else if(command==11)
                    login();
                else if (command == 12){
                    System.out.println("Ceau Ceau");
                    break;
            }
                else
                    System.out.println("Invalid command");
            } catch(NumberFormatException ex){
                System.out.println("The command should be a number!\n");
            }

        }
    }
}
